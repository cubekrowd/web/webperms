package net.cubekrowd.webperms;

import java.util.*;
import lombok.*;

@Data
public class BanProfile {

    private final UUID uniqueId;
    private final String name;
    private CurrentBan currentBan = null;
    private CurrentMute currentMute = null;
    private List<PastBan> pastBans = new ArrayList<>();
    private List<PastMute> pastMutes = new ArrayList<>();
    private List<Warning> warnings = new ArrayList<>();

    @Data
    @AllArgsConstructor
    public static class CurrentBan {
        private int id;
        private String reason;
        private UUID bannedByUniqueId;
        private String bannedByName;
        private int created;
        private int expires;
    }

    @Data
    @AllArgsConstructor
    public static class PastBan {
        private int id;
        private String reason;
        private UUID unbannedByUniqueId;
        private String unbannedByName;
        private UUID bannedByUniqueId;
        private String bannedByName;
        private int created;
        private int expired;
    }

    @Data
    @AllArgsConstructor
    public static class Warning {
        private int id;
        private String reason;
        private UUID warnedByUniqueId;
        private String warnedByName;
        private int created;
    }

    @Data
    @AllArgsConstructor
    public static class CurrentMute {
        private int id;
        private String reason;
        private UUID mutedByUniqueId;
        private String mutedByName;
        private int created;
        private int expires;
    }

    @Data
    @AllArgsConstructor
    public static class PastMute {
        private int id;
        private String reason;
        private UUID unmutedByUniqueId;
        private String unmutedByName;
        private UUID mutedByUniqueId;
        private String mutedByName;
        private int created;
        private int expired;
    }

}