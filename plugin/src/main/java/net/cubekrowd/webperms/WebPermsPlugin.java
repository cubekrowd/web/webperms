package net.cubekrowd.webperms;

import java.util.*;
import java.util.Date;
import java.util.stream.*;
import static spark.Spark.*;
import net.md_5.bungee.api.plugin.*;
import spark.Spark;
import net.cubekrowd.webperms.BanProfile.*;
import net.luckperms.api.*;

import java.nio.ByteBuffer;
import java.sql.*;
import lombok.*;

public class WebPermsPlugin extends Plugin {

    private final List<UUID> boss = Stream.of("a8e85135-fc6d-483e-b2d2-28e1057557f1", // CK
            "76c47dc4-4042-4411-909c-adf2681850f4", // JL
            "c6da74e2-1ea2-46c2-bdae-78e591f7ab51") // Cube
            .map(s -> UUID.fromString(s)).collect(Collectors.toList());

    private LuckPerms luckPerms;

    // This replaces the BanManager console (which is a non-existent player) with an
    // actual player so UUID-lookups/avatars/etc doesn't break.
    private final static UUID BANMANAGER_CONSOLE_UUID = UUID.fromString("1cda7690-b3d9-47b4-b339-1fa5fd012b31");
    private final static UUID BANMANAGER_CONSOLE_REPLACE = UUID.fromString("f78a4d8d-d51b-4b39-98a3-230f2de0c670");

    @SneakyThrows
    private Connection connectBanManagerDB() {
        // This will load the MySQL driver, each DB has its own driver
        Class.forName("com.mysql.jdbc.Driver");
        return DriverManager.getConnection(System.getenv("BANMANAGER_CONNECTION_URL"));
    }

    @Override
    public void onEnable() {
        luckPerms = LuckPermsProvider.get();

        before((req, res) -> {
            getLogger().info(req.ip() + "/" + req.headers("X-Real-IP") + " - [" + (new Date()).toString() + "] \""
                    + req.requestMethod() + " " + req.url() + "\" " + req.userAgent());
        });

        get("/api/v1/staff", (req, res) -> {
            List<String> l = new ArrayList<>();
            l.add("owners:");
            l.add("  - uuid: \"76c47dc440424411909cadf2681850f4\"");
            l.add("    name: \"JL2579\"");
            l.add("    full: true");
            l.add("  - uuid: \"c6da74e21ea246c2bdae78e591f7ab51\"");
            l.add("    name: \"Cubehamster\"");
            l.add("    full: false");
            printStaffGroup(l, "admin", true);
            printStaffGroup(l, "developer", true);
            printStaffGroup(l, "plugindev", false);
            printStaffGroup(l, "moderator", true);
            printStaffGroup(l, "helper", true);
            return String.join("\r\n", l);
        });

        // Note: NOT used by the web UI
        // Only used manually by staff
        get("/api/v1/bans/list", (req, res) -> {
            List<String> l = new ArrayList<>();
            var nlt = getBanManagerNameLookupTable();
            res.type("text/html");
            printAllBans(l, nlt);
            return String.join("<br />", l);
        });

        get("/api/v1/bans/recent", (req, res) -> {
            List<String> l = new ArrayList<>();
            var nlt = getBanManagerNameLookupTable();
            printRecentBans(l, nlt);
            printRecentWarnings(l, nlt);
            return String.join("\r\n", l);
        });

        get("/api/v1/bans/users", (req, res) -> {
            List<String> l = new ArrayList<>();
            var nlt = getBanManagerNameLookupTable();
            printBanProfiles(l, nlt);
            return String.join("\r\n", l);
        });

        get("/api/v1/bans/history", (req, res) -> {
            List<String> l = new ArrayList<>();
            var nlt = getBanManagerNameLookupTable();
            printHistory(l, nlt);
            return String.join("\r\n", l);
        });
    }

    @SneakyThrows
    public void printStaffGroup(List<String> l, String name, boolean title) {
        if (title) {
            l.add(name + "s:");
        }
        luckPerms.getUserManager().getWithPermission("group." + name).get().stream().map(hp -> hp.getHolder())
                .filter(u -> !boss.contains(u)).map(u -> u2n(u) + ";" + u.toString())
                .sorted((s1, s2) -> s1.toLowerCase().compareTo(s2.toLowerCase())).map(s -> s.split(";")).forEach(sa -> {
                    l.add("  - uuid: \"" + sa[1] + "\"");
                    l.add("    name: \"" + sa[0] + "\"");
                });
    }

    @SneakyThrows
    public String u2n(UUID u) {
        return luckPerms.getUserManager().loadUser(u).get().getUsername();
    }

    @SneakyThrows
    private void printRecentBans(List<String> l, Map<UUID, String> nameLookupTable) {
        var conn = connectBanManagerDB();
        var stmt = conn.createStatement();
        var result = stmt.executeQuery("SELECT * FROM `bm_player_bans` ORDER BY `bm_player_bans`.`id` DESC LIMIT 10");
        l.add("recentBans:");
        while (result.next()) {
            UUID targetUUID = bmp2uuid(result.getBytes("player_id"));
            if (targetUUID == BANMANAGER_CONSOLE_UUID) {
                targetUUID = BANMANAGER_CONSOLE_REPLACE;
            }
            UUID actorUUID = bmp2uuid(result.getBytes("actor_id"));
            if (actorUUID == BANMANAGER_CONSOLE_UUID) {
                actorUUID = BANMANAGER_CONSOLE_REPLACE;
            }
            l.add("  - id: " + result.getInt("id"));
            l.add("    target_uuid: \"" + targetUUID.toString() + "\"");
            l.add("    target_name: \"" + nameLookupTable.get(targetUUID) + "\"");
            l.add("    reason: '" + result.getString("reason").replaceAll("'", "") + "'");
            l.add("    actor_uuid: \"" + actorUUID.toString() + "\"");
            l.add("    actor_name: \"" + nameLookupTable.get(actorUUID) + "\"");
            l.add("    created: " + result.getInt("created") + "");
            l.add("    expires: " + result.getInt("expires") + "");
        }
        result.close();
        stmt.close();
        conn.close();
    }

    @SneakyThrows
    private void printHistory(List<String> l, Map<UUID, String> nameLookupTable) {
        var conn = connectBanManagerDB();
        var stmt = conn.createStatement();
        var result = stmt.executeQuery(
                "SELECT `player_id`, `actor_id`, `reason`, `created`, `expires`, 'ban' as `type` FROM `bm_player_bans` " +
                "UNION SELECT `player_id`, `pastActor_id`, `reason`, `pastCreated`, `expired`, 'ban' as `type` FROM `bm_player_ban_records` " +
                "UNION SELECT `player_id`, `actor_id`, `reason`, `created`, `expires`, 'mute' as `type` FROM `bm_player_mutes` " +
                "UNION SELECT `player_id`, `pastActor_id`, `reason`, `pastCreated`, `expired`, 'mute' as `type` FROM `bm_player_mute_records` " +
                "UNION SELECT `player_id`, `actor_id`, `reason`, `created`, 0 as `expires`, 'warning' as `type` FROM `bm_player_warnings` " +
                "ORDER BY `created` DESC");
        while (result.next()) {
            UUID targetUUID = bmp2uuid(result.getBytes("player_id"));
            if (targetUUID == BANMANAGER_CONSOLE_UUID) {
                targetUUID = BANMANAGER_CONSOLE_REPLACE;
            }
            l.add("  - target_uuid: \"" + targetUUID.toString() + "\"");
            l.add("    target_name: \"" + nameLookupTable.get(targetUUID) + "\"");
            l.add("    reason: '" + result.getString("reason").replaceAll("'", "") + "'");
            UUID actorUUID = bmp2uuid(result.getBytes("actor_id"));
            if (actorUUID == BANMANAGER_CONSOLE_UUID) {
                actorUUID = BANMANAGER_CONSOLE_REPLACE;
            }
            l.add("    actor_uuid: \"" + actorUUID.toString() + "\"");
            l.add("    actor_name: \"" + nameLookupTable.get(actorUUID) + "\"");
            l.add("    created: " + result.getInt("created") + "");
            l.add("    expires: " + result.getInt("expires") + "");
            l.add("    type: " + result.getString("type") + "");
        }
        result.close();
        stmt.close();
        conn.close();
    }

    // Note: NOT used by the web UI
    // Only used manually by staff
    @SneakyThrows
    private void printAllBans(List<String> l, Map<UUID, String> nameLookupTable) {
        var conn = connectBanManagerDB();
        var stmt = conn.createStatement();
        var result = stmt.executeQuery(
                "SELECT `player_id`, `reason`, `created` FROM `bm_player_bans` UNION SELECT `player_id`, `reason`, `created` FROM `bm_player_ban_records` ORDER BY `created`");
        while (result.next()) {
            UUID targetUUID = bmp2uuid(result.getBytes("player_id"));
            if (targetUUID == BANMANAGER_CONSOLE_UUID) {
                targetUUID = BANMANAGER_CONSOLE_REPLACE;
            }
            Date d = new Date(result.getInt("created") * 1000L);
            l.add(d.toString() + " &emsp; " + nameLookupTable.get(targetUUID) + " &emsp; "
                    + result.getString("reason").replaceAll("'", ""));
        }
        result.close();
        stmt.close();
        conn.close();
    }

    @SneakyThrows
    private void printRecentWarnings(List<String> l, Map<UUID, String> nameLookupTable) {
        var conn = connectBanManagerDB();
        var stmt = conn.createStatement();
        var result = stmt
                .executeQuery("SELECT * FROM `bm_player_warnings` ORDER BY `bm_player_warnings`.`id` DESC LIMIT 10");
        l.add("recentWarnings:");
        while (result.next()) {
            l.add("  - id: " + result.getInt("id"));
            UUID targetUUID = bmp2uuid(result.getBytes("player_id"));
            if (targetUUID == BANMANAGER_CONSOLE_UUID) {
                targetUUID = BANMANAGER_CONSOLE_REPLACE;
            }
            l.add("    target_uuid: \"" + targetUUID.toString() + "\"");
            l.add("    target_name: \"" + nameLookupTable.get(targetUUID) + "\"");
            l.add("    reason: '" + result.getString("reason").replaceAll("'", "") + "'");
            UUID actorUUID = bmp2uuid(result.getBytes("actor_id"));
            if (actorUUID == BANMANAGER_CONSOLE_UUID) {
                actorUUID = BANMANAGER_CONSOLE_REPLACE;
            }
            l.add("    actor_uuid: \"" + actorUUID.toString() + "\"");
            l.add("    actor_name: \"" + nameLookupTable.get(actorUUID) + "\"");
            l.add("    created: " + result.getInt("created") + "");
        }
        result.close();
        stmt.close();
        conn.close();
    }

    @SneakyThrows
    private void printBanProfiles(List<String> l, Map<UUID, String> nameLookupTable) {
        Map<UUID, BanProfile> banProfiles = new HashMap<>();

        populateBanProfilesCurrentBans(banProfiles, nameLookupTable);
        populateBanProfilesPastBans(banProfiles, nameLookupTable);
        populateBanProfilesWarnings(banProfiles, nameLookupTable);
        populateBanProfilesCurrentMutes(banProfiles, nameLookupTable);
        populateBanProfilesPastMutes(banProfiles, nameLookupTable);

        for (BanProfile profile : banProfiles.values()) {
            l.add("- uuid: \"" + profile.getUniqueId() + "\"");
            l.add("  username: \"" + profile.getName() + "\"");
            if (profile.getCurrentBan() != null) {
                l.add("  currentBan:");
                CurrentBan ban = profile.getCurrentBan();
                l.add("      id: " + ban.getId());
                l.add("      reason: '" + ban.getReason().replaceAll("'", "") + "'");
                l.add("      bannedby_uuid: \"" + ban.getBannedByUniqueId() + "\"");
                l.add("      bannedby_name: \"" + ban.getBannedByName() + "\"");
                l.add("      created: " + ban.getCreated());
                l.add("      expires: " + ban.getExpires());
            }
            if (profile.getCurrentMute() != null) {
                l.add("  currentMute:");
                CurrentMute mute = profile.getCurrentMute();
                l.add("      id: " + mute.getId());
                l.add("      reason: '" + mute.getReason().replaceAll("'", "") + "'");
                l.add("      mutedby_uuid: \"" + mute.getMutedByUniqueId() + "\"");
                l.add("      mutedby_name: \"" + mute.getMutedByName() + "\"");
                l.add("      created: " + mute.getCreated());
                l.add("      expires: " + mute.getExpires());
            }
            l.add("  pastBans:");
            for (var ban : profile.getPastBans()) {
                l.add("      - id: " + ban.getId());
                l.add("        reason: '" + ban.getReason().replaceAll("'", "") + "'");
                l.add("        unbannedby_uuid: \"" + ban.getUnbannedByUniqueId() + "\"");
                l.add("        unbannedby_name: \"" + ban.getUnbannedByName() + "\"");
                l.add("        bannedby_uuid: \"" + ban.getBannedByUniqueId() + "\"");
                l.add("        bannedby_name: \"" + ban.getBannedByName() + "\"");
                l.add("        created: " + ban.getCreated());
                l.add("        expired: " + ban.getExpired());
            }
            l.add("  pastMutes:");
            for (var mute : profile.getPastMutes()) {
                l.add("      - id: " + mute.getId());
                l.add("        reason: '" + mute.getReason().replaceAll("'", "") + "'");
                l.add("        unmutedby_uuid: \"" + mute.getUnmutedByUniqueId() + "\"");
                l.add("        unmutedby_name: \"" + mute.getUnmutedByName() + "\"");
                l.add("        mutedby_uuid: \"" + mute.getMutedByUniqueId() + "\"");
                l.add("        mutedby_name: \"" + mute.getMutedByName() + "\"");
                l.add("        created: " + mute.getCreated());
                l.add("        expired: " + mute.getExpired());
            }
            l.add("  warnings:");
            for (var warn : profile.getWarnings()) {
                l.add("      - id: " + warn.getId());
                l.add("        reason: '" + warn.getReason().replaceAll("'", "") + "'");
                l.add("        warnedby_uuid: \"" + warn.getWarnedByUniqueId() + "\"");
                l.add("        warnedby_name: \"" + warn.getWarnedByName() + "\"");
                l.add("        created: " + warn.getCreated());
            }
        }
    }

    @SneakyThrows
    private void populateBanProfilesCurrentBans(Map<UUID, BanProfile> banProfiles, Map<UUID, String> nameLookupTable) {
        var conn = connectBanManagerDB();
        var stmt = conn.createStatement();
        var result = stmt.executeQuery("SELECT * FROM `bm_player_bans`");
        while (result.next()) {
            UUID uuid = bmp2uuid(result.getBytes("player_id"));
            UUID bannedById = bmp2uuid(result.getBytes("actor_id"));
            if (bannedById == BANMANAGER_CONSOLE_UUID) {
                bannedById = BANMANAGER_CONSOLE_REPLACE;
            }
            CurrentBan ban = new CurrentBan(result.getInt("id"), result.getString("reason"), bannedById,
                    nameLookupTable.get(bannedById), result.getInt("created"), result.getInt("expires"));
            if (!banProfiles.containsKey(uuid)) {
                banProfiles.put(uuid, new BanProfile(uuid, nameLookupTable.get(uuid)));
            }
            banProfiles.get(uuid).setCurrentBan(ban);
        }
        result.close();
        stmt.close();
        conn.close();
    }

    @SneakyThrows
    private void populateBanProfilesCurrentMutes(Map<UUID, BanProfile> banProfiles, Map<UUID, String> nameLookupTable) {
        var conn = connectBanManagerDB();
        var stmt = conn.createStatement();
        var result = stmt.executeQuery("SELECT * FROM `bm_player_mutes`");
        while (result.next()) {
            UUID uuid = bmp2uuid(result.getBytes("player_id"));
            UUID mutedById = bmp2uuid(result.getBytes("actor_id"));
            if (mutedById == BANMANAGER_CONSOLE_UUID) {
                mutedById = BANMANAGER_CONSOLE_REPLACE;
            }
            CurrentMute mute = new CurrentMute(result.getInt("id"), result.getString("reason"), mutedById,
                    nameLookupTable.get(mutedById), result.getInt("created"), result.getInt("expires"));
            if (!banProfiles.containsKey(uuid)) {
                banProfiles.put(uuid, new BanProfile(uuid, nameLookupTable.get(uuid)));
            }
            banProfiles.get(uuid).setCurrentMute(mute);
        }
        result.close();
        stmt.close();
        conn.close();
    }

    /*
     * PastBans works in the way that BanManager has one table for 'current' bans,
     * and then another table for "unban-records", so it logs when a player was
     * unbanned.
     *
     * All temp bans are always "unbanned" in that they are "unbanned" by Console
     * when their ban expires. See `console.yml` in the BanManager plugin folder.
     */
    @SneakyThrows
    private void populateBanProfilesPastBans(Map<UUID, BanProfile> banProfiles, Map<UUID, String> nameLookupTable) {
        var conn = connectBanManagerDB();
        var stmt = conn.createStatement();
        var result = stmt.executeQuery("SELECT * FROM `bm_player_ban_records`");
        while (result.next()) {
            UUID uuid = bmp2uuid(result.getBytes("player_id"));
            UUID bannedById = bmp2uuid(result.getBytes("pastActor_id"));
            UUID unbannedById = bmp2uuid(result.getBytes("actor_id"));
            if (bannedById == BANMANAGER_CONSOLE_UUID) {
                bannedById = BANMANAGER_CONSOLE_REPLACE;
            }
            if (unbannedById == BANMANAGER_CONSOLE_UUID) {
                unbannedById = BANMANAGER_CONSOLE_REPLACE;
            }
            PastBan ban = new PastBan(result.getInt("id"), result.getString("reason"), unbannedById,
                    nameLookupTable.get(unbannedById), bannedById, nameLookupTable.get(bannedById),
                    result.getInt("pastCreated"), result.getInt("created"));
            if (!banProfiles.containsKey(uuid)) {
                banProfiles.put(uuid, new BanProfile(uuid, nameLookupTable.get(uuid)));
            }
            banProfiles.get(uuid).getPastBans().add(ban);
        }
        result.close();
        stmt.close();
        conn.close();
    }

    @SneakyThrows
    private void populateBanProfilesPastMutes(Map<UUID, BanProfile> banProfiles, Map<UUID, String> nameLookupTable) {
        var conn = connectBanManagerDB();
        var stmt = conn.createStatement();
        var result = stmt.executeQuery("SELECT * FROM `bm_player_mute_records`");
        while (result.next()) {
            UUID uuid = bmp2uuid(result.getBytes("player_id"));
            UUID mutedById = bmp2uuid(result.getBytes("pastActor_id"));
            UUID unmutedById = bmp2uuid(result.getBytes("actor_id"));
            if (mutedById == BANMANAGER_CONSOLE_UUID) {
                mutedById = BANMANAGER_CONSOLE_REPLACE;
            }
            if (unmutedById == BANMANAGER_CONSOLE_UUID) {
                unmutedById = BANMANAGER_CONSOLE_REPLACE;
            }
            PastMute mute = new PastMute(result.getInt("id"), result.getString("reason"), unmutedById,
                    nameLookupTable.get(unmutedById), mutedById, nameLookupTable.get(mutedById),
                    result.getInt("pastCreated"), result.getInt("created"));
            if (!banProfiles.containsKey(uuid)) {
                banProfiles.put(uuid, new BanProfile(uuid, nameLookupTable.get(uuid)));
            }
            banProfiles.get(uuid).getPastMutes().add(mute);
        }
        result.close();
        stmt.close();
        conn.close();
    }

    @SneakyThrows
    private void populateBanProfilesWarnings(Map<UUID, BanProfile> banProfiles, Map<UUID, String> nameLookupTable) {
        var conn = connectBanManagerDB();
        var stmt = conn.createStatement();
        var result = stmt.executeQuery("SELECT * FROM `bm_player_warnings`");
        while (result.next()) {
            UUID uuid = bmp2uuid(result.getBytes("player_id"));
            UUID warnedById = bmp2uuid(result.getBytes("actor_id"));
            if (warnedById == BANMANAGER_CONSOLE_UUID) {
                warnedById = BANMANAGER_CONSOLE_REPLACE;
            }
            Warning warning = new Warning(result.getInt("id"), result.getString("reason"), warnedById,
                    nameLookupTable.get(warnedById), result.getInt("created"));
            if (!banProfiles.containsKey(uuid)) {
                banProfiles.put(uuid, new BanProfile(uuid, nameLookupTable.get(uuid)));
            }
            banProfiles.get(uuid).getWarnings().add(warning);
        }
        result.close();
        stmt.close();
        conn.close();
    }

    // Ban Manager Player
    // 0x7e1b2286e79446fdba8bf806dc7062e1
    public UUID bmp2uuid(byte[] bytes) {
        ByteBuffer bb = ByteBuffer.wrap(bytes);
        return new UUID(bb.getLong(), bb.getLong());
    }

    @SneakyThrows
    public Map<UUID, String> getBanManagerNameLookupTable() {
        var conn = connectBanManagerDB();
        var stmt = conn.createStatement();
        var result = stmt.executeQuery("SELECT * FROM `bm_players`");
        Map<UUID, String> nameLookupTable = new HashMap<>();
        while (result.next()) {
            nameLookupTable.put(bmp2uuid(result.getBytes("id")), result.getString("name"));
        }
        result.close();
        stmt.close();
        conn.close();
        nameLookupTable.put(BANMANAGER_CONSOLE_UUID, "CONSOLE");
        return nameLookupTable;
    }

    @Override
    @SneakyThrows
    public void onDisable() {
        luckPerms = null;
    }

}
