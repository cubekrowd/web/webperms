FROM maven:3-openjdk-17
ADD plugin /opt/plugin
RUN cd /opt/plugin && mvn package -B

FROM openjdk:17-jdk-slim

# Add server files
ADD server /opt/server
COPY --from=0 /opt/plugin/target/webperms.jar /opt/server/plugins/webperms.jar

# Create group and users
RUN groupadd -g 1008 serverstaff && \
    useradd -g serverstaff -u 1000 -r -M minecraft && \
    mkdir /opt/minecraft /opt/server/logs && \
    chown minecraft:serverstaff /opt/server /opt/minecraft -R && \
    chmod ugo+rwx /opt/server /opt/minecraft -R

# Start the server
WORKDIR /opt/server
EXPOSE 4567
USER minecraft:serverstaff
CMD ["java","-DIReallyKnowWhatIAmDoingISwear","-server","-Xmx500M","-Dlog4j2.formatMsgNoLookups=true","-jar","Waterfall.jar"]
